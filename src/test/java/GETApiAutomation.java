import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GETApiAutomation {
    @Test
    public void listOfDresses(){

        Response dressDetailsResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .get("https://notchabove.in/collections/dresses/products/ruby-dress");

        int statusCode =  dressDetailsResponse.statusCode();

        Assert.assertTrue(statusCode==200);

        System.out.println(dressDetailsResponse.body().asString());
    }
}
