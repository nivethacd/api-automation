import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class POSTApiAutomation {
    @Test
    public void postUserDetails(){

        Response addingUserDetailsResponse =  given()
                .when()
                .contentType(ContentType.JSON)
                .body("[\n" +
                        "    {\n" +
                        "        \"id\": 16,\n" +
                        "        \"name\": \"Nivetha Tina\",\n" +
                        "        \"username\": \"nivetina\",\n" +
                        "        \"email\": \"nivetha@nct.com\"\n" +
                        "    }\n" +
                        "]")
                .post("https://jsonplaceholder.typicode.com/users");

        Assert.assertTrue(addingUserDetailsResponse.getStatusCode()==201);

        System.out.println(addingUserDetailsResponse.body().asString());
    }
}
