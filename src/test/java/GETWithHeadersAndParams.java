import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class GETWithHeadersAndParams {
    @Test
    public void listOfUsers(){

        Response userDetailsResponse = given()
                .when()
                .contentType(ContentType.JSON)
                .headers("User-Agent","PostmanRuntime/7.28.2")
                .headers("Content-Encoding", "br")
                .headers("Content-Type", "application/json; charset=utf-8")
                .queryParam("id", "10")
                .get("https://reqres.in/api/users?page=2");

        int statusCode =  userDetailsResponse.statusCode();

        Assert.assertTrue(statusCode==200);

        System.out.println(userDetailsResponse.body().asString());
    }
}